+++
title = "M1.3 Source files for 3D models must be supplied"
+++

The KiCad library maintains a repository for 3D source files at link:https://gitlab.com/kicad/libraries/kicad-packages3D-source[https://gitlab.com/kicad/libraries/kicad-packages3D-source].

The _source files_ used to generate 3D model data must be submitted to this repository when a user contributes 3D model data.

* Source files of 3d models are the native files generated by the 3d modeling software (Example: fcstd for Freecad).

* Source files alternatively are any generator scripts. 
** However scripts found in link:https://github.com/easyw/kicad-3d-models-in-freecad[https://github.com/easyw/kicad-3d-models-in-freecad] do not need to be added to the source repo.

* The source repository directory structure mirrors that of the kicad-packages-3d repository.

**Exceptions**

If the model is being submitted by an authorized representative of the manufacturer of the component, the requirement to
also submit the source files may be waived by the KiCad Library team.  This is contingent on the manufacturer providing the
KiCad team with a primary point of contact to receive issue reports with the model as well as a commitment to providing
updated STEP/WRL models that resolve issues with incorrect STEP/WRL models.

_The KiCad Library team reserves the right to replace any models at their sole discretion and may prioritize models compliant with M1.3._